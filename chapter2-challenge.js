var readline = require('readline');
const { resolve } = require('path');

var nilaiMahasiswa = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

function input(question) {
    return new Promise(resolve => {
        nilaiMahasiswa.question(question, (data) => {
            resolve(data);
        }); 
    });
}

async function main() {
        try {
        let nilai = [];
        let repeat = true;

        while (repeat) {
            const inputNilai = await input('masukkan nilai : ');
            if (inputNilai == 'q') {
                console.log("Nilai tertinggi : " + tertinggi(nilai));
                console.log("Nilai terendah : " + terendah(nilai));
                console.log("Nilai rata- rata : " + reRata(nilai));
                console.log("Siswa lulus : " + lulus(nilai) + ", siswa tidak lulus : " + tidakLulus(nilai));
                console.log("Urutan nilai siswa : " + urutanNilai(nilai));
                console.log("Cari dan tampilkan siswa nilai = 90 dan nilai = 100 : " + nilaiSiswa(nilai));
                repeat = false;
                nilaiMahasiswa.close();
            } else if (isNaN(inputNilai)) {
                console.log('Tolong masukkan angka saja!');
            } else {
                nilai.push(+inputNilai)
            }
        }
    } catch (err) {
        console.log(`Ada error : ${err.message}`);
    }
}
main();


// 1. Nilai tertinggi & terendah 
const tertinggi = (nilai) => {
    let tertinggi = -Infinity;
    for (let x = 0; x < nilai.length; x++) {
        if (tertinggi < nilai[x]){
            tertinggi=nilai[x];
        }
    }
    return tertinggi;
}

const terendah = function (nilai) {
    let terendah = Infinity;
    for (let x = 0; x < nilai.length; x++) {
        if(terendah > nilai[x])
        terendah=nilai[x];
    }
    return terendah;
}


// 2. Mencari rata-rata
const reRata = function (nilai) {
    let total = 0;
    for (let x = 0; x < nilai.length; x++) {
        total += nilai[x];
    }
    return total / nilai.length;
}


// 3. Jumlah Siswa lulus / tidak lulus ujian dengan kriteria nilai lulus harus >= 60
const lulus = function (nilai) {
    let siswaLulus = 0;
    nilai.forEach((nilaiSiswa) => {
        if (nilaiSiswa >= 60) {
            siswaLulus++;
        }
    });
    return siswaLulus;
}

const tidakLulus = function (nilai) {
    let siswaTidakLulus = 0;
    nilai.forEach((nilaiSiswa) => {
        if (nilaiSiswa < 60) {
            siswaTidakLulus++;
        }
    });
    return siswaTidakLulus;
}


// 1. Urutkan nilai siswa 
const urutanNilai = function (nilai) {
    let panjang = nilai.length;
    for (let x = panjang-1; x >= 0; x--) {
        for (let y = 1; y <= x; y++) {
            if (nilai[y-1] > nilai[y]) {
                let temp = nilai[y-1];
                nilai[y-1] = nilai[y];
                nilai[y] = temp;
            }
        }
    }
    return nilai;
};


// 2. Cari dan tampilkan siswa nilai=90 dan nilai=100
const nilaiSiswa = (nilai) => {
    let cari = nilai.filter(item => item == 90 || item == 100)
    return cari;
}